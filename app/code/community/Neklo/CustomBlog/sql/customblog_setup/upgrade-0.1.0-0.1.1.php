<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `{$this->getTable('neklo_customblog/news')}` ADD `products` varchar(255) NOT NULL;
");

$installer->endSetup();