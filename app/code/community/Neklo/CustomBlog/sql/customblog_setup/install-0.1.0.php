<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();


$installer->run("
create table `{$this->getTable('neklo_customblog/news')}`
(
	id int auto_increment
		primary key,
	title varchar(255) not null,
	content text not null,
	category varchar(255) not null,
	image_preview varchar(255) not null,
	date_published date not null
)
;

");

$installer->endSetup();

/*$table = $installer->getConnection()
    ->newTable($this->getTable('neklo_customblog/news'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER,null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ))
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ))
    ->addColumn('category', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ))
    ->addColumn('image_preview', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ))
    ->addColumn('date_published', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable' => false,
    ));*/

