<?php

class Neklo_CustomBlog_Block_PostBlock extends Mage_Core_Block_Template
{
    protected function getNews()
    {
        
        if (!$this->_news) {
            $this->_news = Mage::registry('news');
        }
        return $this->_news;
    }
    
    protected function getProductInfo($productId)
    {
        
        $_product = Mage::getModel('catalog/product')->load($productId);
        
        return $_product;
    }
}
