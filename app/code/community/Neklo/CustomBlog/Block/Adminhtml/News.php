<?php

class Neklo_CustomBlog_Block_Adminhtml_News
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'neklo_customblog';
        $this->_headerText = Mage::helper('neklo_customblog')->__('News');
        $this->_addButtonLabel = Mage::helper('neklo_customblog')->__(
            'Add New News'
        );
        parent::__construct();
    }
}
