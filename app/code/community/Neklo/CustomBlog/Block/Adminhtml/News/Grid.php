<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('neklo_customblog/news')->getCollection();
        /* @var $collection Mage_Cms_Model_Mysql4_Block_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $baseUrl = $this->getUrl();
        $this->addColumn(
            'title', array(
                'header' => Mage::helper('neklo_customblog')->__('Title'),
                'align'  => 'left',
                'index'  => 'title',
            )
        );
        
        $this->addColumn(
            'image_preview', array(
                'header'   => Mage::helper('neklo_customblog')->__(
                    'Image Preview'
                ),
                'align'    => 'left',
                'index'    => 'image_preview',
                'renderer' => 'Neklo_CustomBlog_Block_Adminhtml_News_Grid_Renderer_Image',
                'filter'   => false,
                'sortable' => false,
            )
        );
        
        foreach (
            Mage::getModel('neklo_customblog/news')->getCategorys() as $option
        ) {
            $categoryValues[$option['value']] = $option['label'];
        }
        
        $this->addColumn(
            'category', array(
                'header'                    => Mage::helper('neklo_customblog')
                    ->__(
                        'Category'
                    ),
                'align'                     => 'left',
                'index'                     => 'category',
                'renderer'                  => 'Neklo_CustomBlog_Block_Adminhtml_News_Grid_Renderer_Category',
                'type'                      => 'options',
                'options'                   => $categoryValues,
                'filter_condition_callback' => array($this, '_categoryFilter'),
            )
        );
        
        $this->addColumn(
            'date_published', array(
                'header' => Mage::helper('neklo_customblog')->__(
                    'Date Published'
                ),
                'index'  => 'date_published',
                'type'   => 'date',
            )
        );
        return parent::_prepareColumns();
    }
    
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
    
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        
        $this->getCollection()->addStoreFilter($value);
    }
    
    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    protected function _categoryFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $field = $column->getFilterIndex() ? $column->getFilterIndex()
            : $column->getIndex();
        $cond = $column->getFilter()->getCondition();
        $this->getCollection()->addFieldToFilter(
            $field, array('finset' => $cond["eq"])
        );
        return $this;
    }
}
