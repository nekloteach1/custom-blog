<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'      => 'edit_form',
                'action'  => $this->getUrl(
                    '*/*/save',
                    array('id' => $this->getRequest()->getParam('id'))
                ),
                'method'  => 'post',
                'enctype' => 'multipart/form-data',
            )
        );
        
        $model = Mage::registry('neklo_customblog_block');
        if ($model->getBlockId()) {
            $form->addField(
                'id', 'hidden', array(
                    'name' => 'id',
                )
            );
        }
        
        $form->setUseContainer(true);
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
    
}
