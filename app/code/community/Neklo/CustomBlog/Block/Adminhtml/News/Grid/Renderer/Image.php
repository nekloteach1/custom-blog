<?php


class Neklo_CustomBlog_Block_Adminhtml_News_Grid_Renderer_Image
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if (!$row->getImagePreview()) {
            return 'No Image';
        }
        $url = Mage::getBaseUrl('media') . $row->getImagePreview();
        $html
            = "<p align='center'><img src='$url' height='auto' width='100'></p>";
        return $html;
    }
}
