<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('news_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(
            Mage::helper('neklo_customblog')->__('News Information')
        );
    }
    
    public function _prepareLayout()
    {
        $this->addTab(
            'main_tab', array(
                'label' => $this->__('Main'),
                'title' => $this->__('Main'),
                'content' => $this->getLayout()->createBlock(
                    'neklo_customblog/adminhtml_news_edit_tab_main'
                )->toHtml(),
            )
        );
        
        $this->addTab(
            'products', array(
                'label' => Mage::helper('neklo_customblog')->__('Products'),
                'title' => Mage::helper('neklo_customblog')->__('Products'),
                'class' => 'ajax',
                'url'   => $this->getUrl(
                    '*/*/product', array('_current' => true)
                ),
            
            )
        );
        return parent::_prepareLayout();
    }
}
