<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Grid_Renderer_Category
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    
    public function render(Varien_Object $row)
    {
        return $row->getCategory();
    }
}