<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Edit_Tab_Product
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('asc');
        $this->setDefaultFilter(array('in_products' => 1));
        $this->setSaveParametersInSession(false);
    }
    
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_products') {
            $customerIds = $this->getSelectedProducts();
            if (empty($customerIds)) {
                $customerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter(
                    'entity_id', array('in' => $customerIds)
                );
            } else {
                if ($customerIds) {
                    $this->getCollection()->addFieldToFilter(
                        'entity_id', array('nin' => $customerIds)
                    );
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*');
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_products', array(
            'header_css_class' => 'a-center',
            'type'             => 'checkbox',
            'name'             => 'in_products',
            'values'           => $this->getSelectedProducts(),
            'align'            => 'center',
            'index'            => 'entity_id'
        )
        );
        
        $this->addColumn(
            'entity_id', array(
            'header'   => Mage::helper('catalog')->__('ID'),
            'sortable' => true,
            'width'    => 60,
            'index'    => 'entity_id'
        )
        );
        
        $this->addColumn(
            'name', array(
            'header' => $this->__('Name'),
            'index'  => 'name',
        )
        );
        
        $this->addColumn(
            'type', array(
            'header'  => Mage::helper('catalog')->__('Type'),
            'width'   => 100,
            'index'   => 'type_id',
            'type'    => 'options',
            'options' => Mage::getSingleton('catalog/product_type')
                ->getOptionArray(),
        )
        );
        
        $this->addColumn(
            'price', array(
            'header'        => Mage::helper('catalog')->__('Price'),
            'type'          => 'currency',
            'currency_code' => (string)Mage::getStoreConfig(
                Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE
            ),
            'index'         => 'price'
        )
        );
        
        return parent::_prepareColumns();
    }
    
    public function getGridUrl()
    {
        return $this->_getData('grid_url')
            ? $this->_getData('grid_url')
            : $this->getUrl('*/*/productgrid', array('_current' => true));
    }
    
    public function getSelectedProducts()
    {
        $products = array();
        $productsIds = json_decode(
            Mage::getModel('neklo_customblog/news')->load(
                $this->getRequest()->getParam('id')
            )->getData('products')
        );
        
        foreach ($productsIds as $product) {
            $products[] = $product;
        }
        
        return $products;
    }
}