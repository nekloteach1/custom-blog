<?php

class Neklo_CustomBlog_Block_Adminhtml_News_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('neklo_customblog_block');
        $form = new Varien_Data_Form();
        
        $form->setHtmlIdPrefix('block_');
        
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => Mage::helper('neklo_customblog')->__(
                    'General Information'
                ),
                'class'  => 'fieldset-wide'
            )
        );
        $fieldset->addField(
            'category', 'multiselect', array(
                'name'     => 'category[]',
                'label'    => Mage::helper('neklo_customblog')->__('Category'),
                'title'    => Mage::helper('neklo_customblog')->__('Category'),
                'values'   => Mage::getModel('neklo_customblog/news')
                    ->getCategorys(),
                'required' => true,
            
            )
        );
        
        $fieldset->addField(
            'title', 'text', array(
                'name'     => 'title',
                'label'    => Mage::helper('neklo_customblog')->__(
                    'News Title'
                ),
                'title'    => Mage::helper('neklo_customblog')->__(
                    'News Title'
                ),
                'required' => true,
                'maxlength' => 70,
            )
        );
        
        $fieldset->addField(
            'date_published', 'date', array(
                'name'  => 'date_published',
                'label' => Mage::helper('neklo_customblog')->__(
                    'Date Published'
                ),
                
                'tabindex' => 1,
                'image'    => $this->getSkinUrl('images/grid-cal.gif'),
                'format'   => Varien_Date::DATE_INTERNAL_FORMAT,
                'value'    => date(
                    Mage::app()
                        ->getLocale()
                        ->getDateStrFormat(
                            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
                        ),
                    strtotime('next weekday')
                ),
                'required' => true,
                'class'    => 'validate-date validate-date-range date-range-event_date-from',
            )
        );
        
        $fieldset->addType(
            'myimage',
            'Neklo_CustomBlog_Block_Adminhtml_News_Edit_Renderer_Myimage'
        );
        $fieldset->addField(
            'image_preview', 'image', array(
                'label'    => Mage::helper('neklo_customblog')->__(
                    'Image Preview'
                ),
                'required' => false,
                'name'     => 'image_preview',
                'default'  => ''
            )
        );
        
        
        $fieldset->addField(
            'content', 'editor', array(
                'name'     => 'content',
                'label'    => Mage::helper('neklo_customblog')->__('Content'),
                'title'    => Mage::helper('neklo_customblog')->__('Content'),
                'style'    => 'height:36em',
                'required' => true,
                'config'   => Mage::getSingleton('cms/wysiwyg_config')
                    ->getConfig()
            
            )
        );
        
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
}
