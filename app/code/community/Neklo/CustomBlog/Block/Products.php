<?php

class Neklo_CustomBlog_Block_Products
    extends Mage_Catalog_Block_Product_Abstract
{
    protected $_mapRenderer = 'msrp_noform';
    
    protected $_columnCount = 4;
    
    protected $_items;
    
    protected $_itemCollection;
    
    protected $_itemLimits = array();
    
    protected function _prepareData()
    {
        /* @var $product Mage_Catalog_Model_Product */
        /* Make  Custom Collection*/
        $products = Mage::registry('products');
        $this->_itemCollection = Mage::getResourceModel(
            'catalog/product_collection'
        )
            ->AddAttributeToSelect('name')
            ->addAttributeToSelect('price')
            ->addFinalPrice()
            ->addAttributeToSelect('small_image')
            ->addAttributeToSelect('image')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSelect('short_description')
            ->addUrlRewrite()
            ->addFieldToFilter('entity_id', array('‌​eq' => $products));
        
        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')
                ->addExcludeProductFilter(
                    $this->_itemCollection,
                    Mage::getSingleton('checkout/session')->getQuoteId()
                );
            
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
//        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
        Mage::getSingleton('catalog/product_visibility')
            ->addVisibleInCatalogFilterToCollection($this->_itemCollection);
        
        if ($this->getItemLimit('upsell') > 0) {
            $this->_itemCollection->setPageSize($this->getItemLimit('upsell'));
        }
        
        $this->_itemCollection->load();
        
        /**
         * Updating collection with desired items
         */
        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }
        
        return $this;
        
        // Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
        // Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);
        //$this->_itemCollection->load();
        // return $this;
        
    }
    
    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }
    
    public function getItemCollection()
    {
        return $this->_itemCollection;
    }
    
    public function getItems()
    {
        if (is_null($this->_items) && $this->getItemCollection()) {
            $this->_items = $this->getItemCollection()->getItems();
        }
        return $this->_items;
    }
    
    public function getRowCount()
    {
        return ceil(
            count($this->getItemCollection()->getItems())
            / $this->getColumnCount()
        );
    }
    
    public function setColumnCount($columns)
    {
        if (intval($columns) > 0) {
            $this->_columnCount = intval($columns);
        }
        return $this;
    }
    
    public function getColumnCount()
    {
        return $this->_columnCount;
    }
    
    public function resetItemsIterator()
    {
        $this->getItems();
        reset($this->_items);
    }
    
    public function getIterableItem()
    {
        $item = current($this->_items);
        next($this->_items);
        return $item;
    }
    
    /**
     * Set how many items we need to show in upsell block
     * Notice: this parametr will be also applied
     *
     * @param string $type
     * @param int    $limit
     *
     * @return Mage_Catalog_Block_Product_List_Upsell
     */
    public function setItemLimit($type, $limit)
    {
        if (intval($limit) > 0) {
            $this->_itemLimits[$type] = intval($limit);
        }
        return $this;
    }
    
    public function getItemLimit($type = '')
    {
        if ($type == '') {
            return $this->_itemLimits;
        }
        if (isset($this->_itemLimits[$type])) {
            return $this->_itemLimits[$type];
        } else {
            return 0;
        }
    }
    
    /**
     * Get tags array for saving cache
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(), $this->getItemsTags($this->getItems())
        );
    }
    
}