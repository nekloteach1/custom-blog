<?php

class Neklo_CustomBlog_Block_NewsBlock extends Mage_Core_Block_Template
{
    private $_itemPerPage;
    private $_pageFrame = 4;
    private $_curPage = 1;
    
    public function _construct()
    {
        parent::_construct();
        $this->_itemPerPage = Mage::getStoreConfig(
            'neklo_customblog/neklo_customblog_group/neklo_customblog_input',
            Mage::app()->getStore()
        );
    }
    
    public function getCollection()
    {
        $methodName = Mage::registry('method');
        $parametres = Mage::registry('paremetres');
        $collection = Mage::getModel('neklo_customblog/news')->$methodName(
            $parametres
        );
        
        if ($collection != 'null') {
            
            $page = $this->getRequest()->getParam('p');
            if ($page) {
                $this->_curPage = $page;
            }
            $collection->setCurPage($this->_curPage);
            $collection->setPageSize($this->_itemPerPage);
            return $collection;
        }
    }
    
    public function getPagerHtml()
    {
        $methodName = Mage::registry('method');
        $parametres = Mage::registry('paremetres');
        $collection = Mage::getModel('neklo_customblog/news')->$methodName(
            $parametres
        );
        $html = false;
        if ($collection == 'null') {
            return;
        }
        if ($collection->count() > $this->_itemPerPage) {
            $curPage = $this->getRequest()->getParam('p');
            $pager = (int)($collection->count() / $this->_itemPerPage);
            $count = ($collection->count() % $this->_itemPerPage == 0) ? $pager
                : $pager + 1;
            $url = $this->getPagerUrl();
            $start = 1;
            $end = $this->_pageFrame;
            $html .= '<ol>';
            if (isset($curPage) && $curPage != 1) {
                $start = $curPage - 1;
                $end = $start + $this->_pageFrame;
            } else {
                $end = $start + $this->_pageFrame;
            }
            if ($end > $count) {
                $start = $count - ($this->_pageFrame - 1);
            } else {
                $count = $end - 1;
            }
            if ($curPage != 1 and $curPage) {
                $html .= '<li><a href="' . $url . '?p=' . 1 . '"><<</a></li>';
                $html .= '<li><a href="' . $url . '?p=' . ($curPage - 1)
                    . '"><</a></li>';
                
            }
            for ($i = $start; $i <= $count; $i++) {
                if ($i >= 1) {
                    if ($curPage) {
                        $html .= ($curPage == $i) ? '<li class="current">' . $i
                            . '</li>'
                            : '<li><a href="' . $url . '?p=' . $i . '">' . $i
                            . '</a></li>';
                    } else {
                        $html .= ($i == 1) ? '<li class="current">' . $i
                            . '</li>'
                            : '<li><a href="' . $url . '?p=' . $i . '">' . $i
                            . '</a></li>';
                    }
                }
            }
            if ($curPage != $count) {
                if (!$curPage) {
                    $html .= '<li><a href="' . $url . '?p=' . 2
                        . '">></a></li>';
                    $html .= '<li><a href="' . $url . '?p=' . ($pager+1)
                        . '">>></a></li>';
                } else {
                    $html .= '<li><a href="' . $url . '?p=' . ($curPage + 1)
                        . '">></a></li>';
                    $html .= '<li><a href="' . $url . '?p=' . ($pager+1)
                        . '">>></a></li>';
                }
                
            }
            $html .= '</ol>';
        }
        return $html;
    }
    
    public function getPagerUrl()
    {
        $curUrl = mage::helper('core/url')->getCurrentUrl();
        $newUrl = preg_replace('/\?p=.*/', '', $curUrl);
        
        return $newUrl;
    }
    
}