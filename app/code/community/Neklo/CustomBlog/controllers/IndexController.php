<?php

class Neklo_CustomBlog_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if (!$this->getRequest()->getParam('name')) {
            $newsModel = Mage::getModel('neklo_customblog/news');
            // $news = $newsModel->getAllNews();
            $categorys = $newsModel->getCategorys();
            Mage::register('method', 'getAllNews');
            Mage::register('paremetres', '');
            Mage::register('categorys', $categorys);
            $this->loadLayout();
            $this->renderLayout();
        }
    }
    
    public function showAction()
    {
        $newsId = $this->getRequest()->getParam('id');
        Mage::register(
            'products', json_decode(
                Mage::getModel('neklo_customblog/news')->load($newsId)
                    ->getProducts()
            )
        );
        $newsModel = Mage::getModel('neklo_customblog/news');
        $newsList = $newsModel->load($newsId);
        $categorys = $newsModel->getCategorys();
        Mage::register('categorys', $categorys);
        Mage::register('news', $newsList);
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function categoryAction()
    {
        $categoryName = $this->getRequest()->getParam('name');
        $newsModel = Mage::getModel('neklo_customblog/news');
        //$newsList['news'] = $newsModel->getNewsByCategoryName($categoryName);
        $categorys = $newsModel->getCategorys();
        Mage::register('method', 'getNewsByCategoryName');
        Mage::register('paremetres', $categoryName);
        Mage::register('categorys', $categorys);
        $this->loadLayout();
        $this->renderLayout();
    }
}