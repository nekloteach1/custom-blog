<?php

class Neklo_CustomBlog_Adminhtml_NewsController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent(
            $this->getLayout()->createBlock('neklo_customblog/adminhtml_news')
        );
        $this->renderLayout();

    }

    public function productAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('product.grid')
            ->setProducts($this->getRequest()->getPost('products', null));
        $this->renderLayout();
    }

    public function productgridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('product.grid')
            ->setProducts($this->getRequest()->getPost('products', null));
        $this->renderLayout();
    }


    public function newAction()
    {

        $this->_forward('edit');

    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        Mage::register(
            'neklo_customblog_block',
            Mage::getModel('neklo_customblog/news')->load($id)
        );
        $this->loadLayout();
        $this->_addLeft(
            $this->getLayout()->createBlock(
                'neklo_customblog/adminhtml_news_edit_tabs'
            )
        );
        $this->_addContent(
            $this->getLayout()->createBlock(
                'neklo_customblog/adminhtml_news_edit'
            )
        );
        $this->renderLayout();
    }

    private function _saveImage($id)
    {
        $uploader = new Varien_File_Uploader('image_preview');
        $uploader->setAllowedExtensions(
            array('jpg', 'jpeg', 'gif', 'png')
        ); // or pdf or anything


        $uploader->setAllowRenameFiles(false);

        // setAllowRenameFiles(true) -> move your file in a folder the magento way
        // setAllowRenameFiles(true) -> move your file directly in the $path folder
        $uploader->setFilesDispersion(false);

        $path = Mage::getBaseDir('media') . DS;
        $imageName = sha1($id) . $_FILES['image_preview']['name'];
        $uploader->save($path, $imageName);
        return $imageName;
    }

    public function saveAction()
    {
        try {
            $id = $this->getRequest()->getParam('id');

            $news = Mage::getModel('neklo_customblog/news')->load($id);

            $news->setData($this->getRequest()->getParams());
            if ($news->getImagePreview()['delete'] == 1) {
                Mage::helper('neklo_customblog')->deleteImageFile(
                    $news->getImagePreview()['value']
                );
                $imageName = '';
            } elseif (isset($_FILES['image_preview']['name']) and (file_exists(
                    $_FILES['image_preview']['tmp_name']
                ))
            ) {

                $imageName = $this->_saveImage(
                    $this->getRequest()->getParam('title')
                );
            } else {
                $imageName = null;
            }

            $news->setImagePreview($imageName);
            $categorys = $news->getCategory();
            $categorys = implode(',', $categorys);
            $news->setCategory($categorys);
            $products = Mage::helper('adminhtml/js')->decodeGridSerializedInput(
                $news->getLinks()['products']
            );
            if ($news->getLinks()['products'] != null) {
                if (count($products) <= 5) {
                    $news->setProducts(json_encode($products));
                    $news->save();
                } else {
                    Mage::getSingleton('adminhtml/session')->addError(
                        'You choose more than 5 products!'
                    );
                    $this->_redirect(
                        '*/*/' . $this->getRequest()->getParam('back', 'index'),
                        array('id' => $news->getId())
                    );
                    return;
                }
            }
            if ($news->getLinks()['products'] == null) {
                $news->save();
            }
            if (!$news->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    'Cannot save the news'
                );
                $this->_redirect(
                    '*/*/' . $this->getRequest()->getParam('back', 'index'),
                    array('id' => $news->getId())
                );
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject(
                $news->getData()
            );
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(
            'News was saved successfully!'
        );
        $this->_redirect(
            '*/*/' . $this->getRequest()->getParam('back', 'index'),
            array('id' => $news->getId())
        );

    }

    public function deleteAction()
    {
        $method = Mage::getModel('neklo_customblog/news')
            ->setId($this->getRequest()->getParam('id'))
            ->delete();
        if ($method->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                'News was deleted successfully!'
            );
        }
        $this->_redirect('*/*/');

    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('news');
    }
}