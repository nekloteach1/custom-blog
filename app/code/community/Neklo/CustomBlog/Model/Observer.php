<?php

class Neklo_CustomBlog_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function addToTopmenu($observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        
        $node = new Varien_Data_Tree_Node(
            array(
                'name' => Mage::helper('neklo_customblog')->__('News'),
                'id'   => 'news',
                'url'  => Mage::getUrl('news'),
            ),
            'id', $tree, $menu
        );
        
        $menu->addChild($node);
        try {
            throw new Exception();
        } catch (Exception $e) {
            Mage::log($e->getTraceAsString(), null, 'observer.log');
        }
        
        Mage::log('checkLogs', null, 'observer.log');
    }
    
    /**
     * @param Varien_Event_Observer $observer
     */
    public function delProductsFromMassChangeStatus($observer)
    {
        Mage::log($observer->getEvent()->getData(), null, 'observer.log');
        if ($observer->getEvent()->getData('attributes_data')['status'] == 2) {
            $allNews = Mage::getModel('neklo_customblog/news')->getCollection();
            foreach ($allNews as $news) {
                $unserialized = json_decode($news['products']);
                $data = $observer->getEvent()->getData('product_ids');
                foreach ($data as $products) {
                    for ($i = 0; $i < count($unserialized); $i++) {
                        if ($unserialized[$i] == $products) {
                            unset($unserialized[$i]);
                            sort($unserialized);
                        }
                    }
                    $news->setProducts(json_encode($unserialized));
                    $news->save();
                }
            }
            
        } else {
            return;
        }
    }
    
    /**
     * @param Varien_Event_Observer $observer
     */
    public function delProductsFromEditProduct($observer)
    {
        
        if ($observer->getProduct()->getOrigData('status') != 2) {
        }
        
        if ($observer->getProduct()->getStatus() == 2) {
            $producId = $observer->getProduct()->getData('entity_id');
            $allNews = Mage::getModel('neklo_customblog/news')->getCollection();
            foreach ($allNews as $news) {
                $unserialized = json_decode($news['products']);
                for ($i = 0; $i < count($unserialized); $i++) {
                    if ($unserialized[$i] == $producId) {
                        unset($unserialized[$i]);
                        sort($unserialized);
                    }
                }
                $news->setProducts(json_encode($unserialized));
                $news->save();
            }
        }
    }
    
    /**
     * @param Varien_Event_Observer $observer
     */
    public function delProductsFromDeleteProduct($observer)
    {
        
        Mage::log($observer->getEvent()->getData(), null, 'observer.log');
        $producId = $observer->getProduct()->getData('entity_id');
        $allNews = Mage::getModel('neklo_customblog/news')->getCollection();
        foreach ($allNews as $news) {
            $unserialized = json_decode($news['products']);
            for ($i = 0; $i < count($unserialized); $i++) {
                if ($unserialized[$i] == $producId) {
                    unset($unserialized[$i]);
                    sort($unserialized);
                }
            }
            $news->setProducts(json_encode($unserialized));
            $news->save();
        }
    }
}