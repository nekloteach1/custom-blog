<?php

class  Neklo_CustomBlog_Model_News extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('neklo_customblog/news');
    }

    public function getAllNews()
    {
        return $this->getCollection()
            ->setOrder('date_published', 'DESC')
            ;
    }

    public function getCategorys()
    {
        $categorys = array(
            array(
                'value' => 'phones',
                'label' => Mage::helper('neklo_customblog')->__('Phones')
            ),
            array(
                'value' => 'technologies',
                'label' => Mage::helper('neklo_customblog')->__('Technologies')
            ),
            array(
                'value' => 'relax_news',
                'label' => Mage::helper('neklo_customblog')->__('Relax news'))
        );
        return $categorys;
    }

    public function getNewsByCategoryName($categoryNames)
    {
        return $this->getCollection()
            ->addFieldToFilter('category', array('finset' => $categoryNames))
            ->setOrder('date_published', 'DESC')
            ;
    }
}