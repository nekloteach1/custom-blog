<?php
class Neklo_CustomBlog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function deleteImageFile($image)
    {
        if (!$image) {
            return;
        }
        try {
            $imgPath = Mage::getBaseDir('media'). DS . $image;
            if (!file_exists($imgPath)) {
                return;
            }
            unlink($imgPath);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}